export function authUser (state) {
  return state.user
}

export function isAdmin (state) {
  return state.user ? state.user.isAdmin : false
}

export function error (state) {
  return state.error
}

export function loading (state) {
  return state.loading
}

export function loggedIn (state) {
  return !!state.user
}

export function guest () {
  const storageItem = window.localStorage.getItem('guest')
  if (!storageItem) return false
  if (storageItem === 'isGuest') return true
  if (storageItem === 'isNotGuest') return false
}
