export function SET_USER (state, user) {
  state.user = user
}

export function SET_LOADING (state, loading) {
  state.loading = loading
}

export function SET_ERROR (state, error) {
  state.error = error
}
