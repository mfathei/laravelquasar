import router from 'src/router'
import AuthService from 'src/services/AuthService'
import {
  getError
} from 'src/utils/helpers'

export function logout (context) {
  return AuthService.logout()
    .then(() => {
      context.commit('SET_USER', null)
      context.dispatch('setGuest', {
        value: 'isGuest'
      })
      if (router.currentRoute.name !== 'login') {
        router.push({
          path: '/login'
        })
      }
    })
    .catch((error) => {
      context.commit('SET_ERROR', getError(error))
    })
}

export async function getAuthUser (context) {
  context.commit('SET_LOADING', true)
  try {
    const response = await AuthService.getAuthUser()
    context.commit('SET_USER', response.data.data)
    context.commit('SET_LOADING', false)
    return response.data.data
  } catch (error) {
    context.commit('SET_LOADING', false)
    context.commit('SET_USER', null)
    context.commit('SET_ERROR', getError(error))
  }
}

export function setGuest (context, {
  value
}) {
  window.localStorage.setItem('guest', value)
}
