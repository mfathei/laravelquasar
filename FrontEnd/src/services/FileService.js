import * as API from 'src/services/API'

export default {
  uploadFile (payload) {
    return API.apiClient.post(payload.endpoint, payload.file)
  }
}
